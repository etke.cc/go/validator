module gitlab.com/etke.cc/go/validator/v2

go 1.18

require (
	blitiri.com.ar/go/spf v1.5.1
	gitlab.com/etke.cc/go/trysmtp v1.1.3
	golang.org/x/net v0.27.0
)
